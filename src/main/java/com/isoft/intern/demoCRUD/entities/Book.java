package com.isoft.intern.demoCRUD.entities;

public class Book {
    private String bookTitle;
    private String bookYear;

    public Book(){}

    public Book(String title, String year) {
        this.bookTitle = title;
        this.bookYear = year;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getBookYear() {
        return bookYear;
    }

    public void setBookYear(String bookYear) {
        this.bookYear = bookYear;
    }
}
