package com.isoft.intern.demoCRUD.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.demoCRUD.entities.Author;
import com.isoft.intern.demoCRUD.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
public class AuthorController {
    private static ObjectMapper objectMapper=new ObjectMapper();
    private final AuthorService authorService;


    @Autowired
    public AuthorController(AuthorService authorService){
        this.authorService = authorService;
    }

    @PostMapping("/author_insert")
    @ResponseBody
    public void insertAuthor(HttpEntity<String> payloadbody) throws IOException {
        String json = payloadbody.getBody();
        Author author= objectMapper.readValue(json, Author.class);
        authorService.insert(author);
    }

    @DeleteMapping("/author_delete")
    @ResponseBody
    public void deleteAuthor(@RequestParam Integer id) {
        authorService.delete(id);
    }

    @PutMapping("author_update")
    @ResponseBody
    public void updateAuthor(HttpEntity<String> payloadbody,@RequestParam Integer id) throws IOException {
        String json = payloadbody.getBody();
        Author author= objectMapper.readValue(json, Author.class);
        authorService.update(author,id);
    }

    @GetMapping("author_get")
    public List<Author> getAuthor(@RequestParam(value="id", required=false) Integer id) throws SQLException {
        if(id!=null)
            return authorService.findById(id);
        else
            return authorService.findAll();

    }
}
