package com.isoft.intern.demoCRUD.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.demoCRUD.entities.Book;
import com.isoft.intern.demoCRUD.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
public class BookController {
    private static ObjectMapper objectMapper=new ObjectMapper();
    private final BookService bookService;


    @Autowired
    public BookController(BookService bookService){
        this.bookService = bookService;
    }

    @PostMapping("/book_insert")
    @ResponseBody
    public void insertBook(HttpEntity<String> payloadbody) throws IOException {
        String json = payloadbody.getBody();
        Book book= objectMapper.readValue(json, Book.class);
        bookService.insert(book);
    }

    @DeleteMapping("/book_delete")
    @ResponseBody
    public void deleteBook(@RequestParam Integer id) {
        bookService.delete(id);
    }

    @PutMapping("book_update")
    @ResponseBody
    public void updateBook(HttpEntity<String> payloadbody,@RequestParam Integer id) throws IOException {
        String json = payloadbody.getBody();
        Book book= objectMapper.readValue(json, Book.class);
        bookService.update(book,id);
    }

    @GetMapping("book_get")
    public List<Book> getBook(@RequestParam(value="id", required=false) Integer id) throws SQLException {
        if(id!=null)
            return bookService.findById(id);
        else
            return bookService.findAll();

    }
}
