package com.isoft.intern.demoCRUD.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoft.intern.demoCRUD.entities.AuthorBook;
import com.isoft.intern.demoCRUD.entities.Book;

import com.isoft.intern.demoCRUD.services.AuthorBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@RestController
public class AuthorBookController {
    private static ObjectMapper objectMapper=new ObjectMapper();
    private final AuthorBookService authorBookService;


    @Autowired
    public AuthorBookController(AuthorBookService authorBookService){
        this.authorBookService = authorBookService;
    }

    @PostMapping("/authorbook_insert")
    @ResponseBody
    public void insertBook(HttpEntity<String> payloadbody) throws IOException {
        String json = payloadbody.getBody();
        AuthorBook authorbook= objectMapper.readValue(json, AuthorBook.class);
        authorBookService.insert(authorbook);
    }

    @DeleteMapping("/authorbook_delete")
    @ResponseBody
    public void deleteBook(@RequestParam Integer id) {
        authorBookService.delete(id);
    }

    @PutMapping("authorbook_update")
    @ResponseBody
    public void updateBook(HttpEntity<String> payloadbody,@RequestParam Integer id) throws IOException {
        String json = payloadbody.getBody();
        AuthorBook authorbook= objectMapper.readValue(json, AuthorBook.class);
        authorBookService.update(authorbook,id);
    }

    @GetMapping("authorbook_get")
    public List<AuthorBook> getBook(@RequestParam(value="id", required=false) Integer id) throws SQLException {
        if(id!=null)
            return authorBookService.findById(id);
        else
            return authorBookService.findAll();

    }
}
