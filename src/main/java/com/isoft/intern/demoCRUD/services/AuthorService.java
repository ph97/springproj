package com.isoft.intern.demoCRUD.services;

import com.isoft.intern.demoCRUD.repositories.AuthorRepositoryImp;
import com.isoft.intern.demoCRUD.entities.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class AuthorService {
    private final AuthorRepositoryImp authorRepositoryImp;

    @Autowired
    public AuthorService(AuthorRepositoryImp authorRepositoryImp){
        this.authorRepositoryImp = authorRepositoryImp;
    }

    public void insert(Author author) {authorRepositoryImp.insert(author);}

    public void delete(Integer id) {authorRepositoryImp.delete(id);}

    public void update(Author author,Integer id) {authorRepositoryImp.update(author,id);}

    public List<Author> findAll() throws SQLException {return authorRepositoryImp.findAll(); }

    public List<Author> findById(Integer id) throws SQLException {return authorRepositoryImp.findById(id);}
}
