package com.isoft.intern.demoCRUD.services;

import com.isoft.intern.demoCRUD.repositories.BookRepositoryImp;
import com.isoft.intern.demoCRUD.entities.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class BookService {
    private final BookRepositoryImp bookRepositoryImp;

    @Autowired
    public BookService(BookRepositoryImp bookRepositoryImp){
        this.bookRepositoryImp = bookRepositoryImp;
    }

    public void insert(Book author) {bookRepositoryImp.insert(author);}

    public void delete(Integer id) {bookRepositoryImp.delete(id);}

    public void update(Book author,Integer id) {bookRepositoryImp.update(author,id);}

    public List<Book> findAll() throws SQLException {return bookRepositoryImp.findAll(); }

    public List<Book> findById(Integer id) throws SQLException {return bookRepositoryImp.findById(id);}
}
