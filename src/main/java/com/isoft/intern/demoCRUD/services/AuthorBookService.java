package com.isoft.intern.demoCRUD.services;

import com.isoft.intern.demoCRUD.entities.AuthorBook;
import com.isoft.intern.demoCRUD.repositories.AuthorBookRepositoryImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class AuthorBookService {
    private final AuthorBookRepositoryImp authorBookRepositoryImp;

    @Autowired
    public AuthorBookService(AuthorBookRepositoryImp authorBookRepositoryImp){
        this.authorBookRepositoryImp = authorBookRepositoryImp;
    }

    public void insert(AuthorBook authorBook) {authorBookRepositoryImp.insert(authorBook);}

    public void delete(Integer id) {authorBookRepositoryImp.delete(id);}

    public void update(AuthorBook authorBook,Integer id) {authorBookRepositoryImp.update(authorBook,id);}

    public List<AuthorBook> findAll() throws SQLException {return authorBookRepositoryImp.findAll(); }

    public List<AuthorBook> findById(Integer id) throws SQLException {return authorBookRepositoryImp.findById(id);}
}
