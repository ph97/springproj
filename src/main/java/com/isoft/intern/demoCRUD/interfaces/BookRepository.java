package com.isoft.intern.demoCRUD.interfaces;

import com.isoft.intern.demoCRUD.entities.Book;

public interface BookRepository extends CRUDRepo<Book,Integer> {
    
}
