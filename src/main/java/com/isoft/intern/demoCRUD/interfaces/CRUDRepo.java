package com.isoft.intern.demoCRUD.interfaces;

import com.sun.xml.internal.bind.v2.model.core.ID;

import java.sql.SQLException;
import java.util.List;

public interface CRUDRepo<E,ID> {

    void insert(E entity);
    void delete(ID id);
    void update(E entity,ID id);

    List<E> findAll() throws SQLException;
    List<E> findById(ID id) throws SQLException;
}
