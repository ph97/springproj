package com.isoft.intern.demoCRUD.interfaces;

import com.isoft.intern.demoCRUD.entities.AuthorBook;


public interface AuthorBookRepository extends CRUDRepo<AuthorBook,Integer> {
    //List<AuthorBookJoined> findAllJoined() throws SQLException;
}
