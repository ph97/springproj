package com.isoft.intern.demoCRUD.interfaces;

import com.isoft.intern.demoCRUD.entities.Author;

public interface AuthorRepository extends CRUDRepo<Author,Integer>{

}
