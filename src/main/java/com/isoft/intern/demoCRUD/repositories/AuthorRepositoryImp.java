package com.isoft.intern.demoCRUD.repositories;

import com.isoft.intern.demoCRUD.entities.Author;
import com.isoft.intern.demoCRUD.interfaces.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

@Repository
public class AuthorRepositoryImp extends JdbcDaoSupport implements AuthorRepository {
    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    public AuthorRepositoryImp() {}

    @Override
    public void insert(Author author) {
        String query = "INSERT INTO author (author_first_name, author_last_name, author_birth_date) "+
                "VALUES (?, ?, ?::date)" ;
        getJdbcTemplate().update(query, new Object[]{author.getFirstName(),author.getLastName(),author.getBirthDate()});
    }

    @Override
    public void delete(Integer id){
        String query = "DELETE FROM author " +
                "WHERE author_id = ?";
        Object[] args = new Object[] {id};

        getJdbcTemplate().update(query, args);
    }

    @Override
    public void update(Author author,Integer id){
        String query = "UPDATE author " +
                "SET author_first_name = ? , author_last_name=? , author_birth_date=?::date "+
                "WHERE author_id = ?";

        getJdbcTemplate().update(query, new Object[]{author.getFirstName(),author.getLastName(),author.getBirthDate(),id});
    }

    @Override
    public List<Author> findAll() throws SQLException {
        String query="SELECT * FROM author";

        return getJdbcTemplate().query(query,new AuthorMapper());
    }

    @Override
    public List<Author> findById(Integer id) throws SQLException {
        String query="SELECT * FROM author "+
                "WHERE author_id=?";
        Author author = getJdbcTemplate().queryForObject(query,new AuthorMapper(),id);
        List<Author> listToBeReturned=new ArrayList<>();
        listToBeReturned.add(author);
        return listToBeReturned;
    }

    private static final class AuthorMapper implements RowMapper<Author> {
        public Author mapRow(ResultSet rs, int rowNum) throws SQLException         {
            Author author = new Author();
            author.setFirstName(rs.getString("author_first_name"));
            author.setLastName(rs.getString("author_last_name"));
            author.setBirthDate(rs.getString("author_birth_date"));
            return author;
        }
    }

}
