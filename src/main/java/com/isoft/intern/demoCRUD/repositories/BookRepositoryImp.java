package com.isoft.intern.demoCRUD.repositories;

import com.isoft.intern.demoCRUD.entities.Book;
import com.isoft.intern.demoCRUD.interfaces.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

@Repository
public class BookRepositoryImp extends JdbcDaoSupport implements BookRepository {
    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    public BookRepositoryImp() {}

    @Override
    public void insert(Book book) {
        String query = "INSERT INTO book (book_title, book_year) "+
                "VALUES (?, ?::INTEGER)" ;
        getJdbcTemplate().update(query, new Object[]{book.getBookTitle(),book.getBookYear()});
    }

    @Override
    public void delete(Integer id){
        String query = "DELETE FROM book " +
                "WHERE book_id = ?";
        Object[] args = new Object[] {id};

        getJdbcTemplate().update(query, args);
    }

    @Override
    public void update(Book book,Integer id){
        String query = "UPDATE book " +
                "SET book_title = ? , book_year=?::INTEGER "+
                "WHERE book_id = ?";

        getJdbcTemplate().update(query, new Object[]{book.getBookTitle(),book.getBookYear(),id});
    }

    @Override
    public List<Book> findAll() throws SQLException {
        String query="SELECT * FROM book";

        return getJdbcTemplate().query(query,new BookMapper());
    }

    @Override
    public List<Book> findById(Integer id) throws SQLException {
        String query="SELECT * FROM book "+
                "WHERE book_id=?";
        Book book = getJdbcTemplate().queryForObject(query,new BookMapper(),id);
        List<Book> listToBeReturned=new ArrayList<>();
        listToBeReturned.add(book);
        return listToBeReturned;
    }

    private static final class BookMapper implements RowMapper<Book> {
        public Book mapRow(ResultSet rs, int rowNum) throws SQLException         {
            Book book = new Book();
            book.setBookTitle(rs.getString("book_title"));
            book.setBookYear(rs.getString("book_year"));
            return book;
        }
    }

}
