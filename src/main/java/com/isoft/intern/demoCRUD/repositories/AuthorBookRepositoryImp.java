package com.isoft.intern.demoCRUD.repositories;

import com.isoft.intern.demoCRUD.entities.Author;
import com.isoft.intern.demoCRUD.entities.AuthorBook;
import com.isoft.intern.demoCRUD.interfaces.AuthorBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

@Repository
public class AuthorBookRepositoryImp extends JdbcDaoSupport implements AuthorBookRepository {
    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize(){
        setDataSource(dataSource);
    }

    public AuthorBookRepositoryImp() {}

    @Override
    public void insert(AuthorBook authorBook) {
        String query = "INSERT INTO author_book (author_id,book_id) "+
                "VALUES (?::integer , ?::integer )" ;
        getJdbcTemplate().update(query, new Object[]{authorBook.getAuthorId(),authorBook.getBookId()});
    }

    @Override
    public void delete(Integer id){
        String query = "DELETE FROM author_book " +
                "WHERE ab_id = ?";
        Object[] args = new Object[] {id};

        getJdbcTemplate().update(query, args);
    }

    @Override
    public void update(AuthorBook authorBook,Integer id){
        String query = "UPDATE author_book " +
                "SET author_id = ?::integer , book_id=?::integer "+
                "WHERE ab_id = ?";

        getJdbcTemplate().update(query, new Object[]{authorBook.getAuthorId(),authorBook.getBookId(),id});
    }

    @Override
    public List<AuthorBook> findAll() throws SQLException {
        String query="SELECT * FROM author_book";

        return getJdbcTemplate().query(query,new AuthorBookMapper());
    }

    @Override
    public List<AuthorBook> findById(Integer id) throws SQLException {
        String query="SELECT * FROM author_book "+
                "WHERE ab_id=?";
        AuthorBook authorBook = getJdbcTemplate().queryForObject(query,new AuthorBookMapper(),id);
        List<AuthorBook> listToBeReturned=new ArrayList<>();
        listToBeReturned.add(authorBook);
        return listToBeReturned;
    }

    private static final class AuthorBookMapper implements RowMapper<AuthorBook> {
        public AuthorBook mapRow(ResultSet rs, int rowNum) throws SQLException         {
            AuthorBook authorBook = new AuthorBook();
            authorBook.setAuthorId(rs.getInt("author_id"));
            authorBook.setBookId(rs.getInt("book_id"));
            return authorBook;
        }
    }

}
